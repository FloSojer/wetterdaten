<div class="container">
    <h2>Messwerte anzeigen</h2>

    <p>
        <a class="btn btn-primary" href="index.php?r=measurement/update&id=<?= $model->getId() ?>">Aktualisieren</a>
        <a class="btn btn-danger" href="index.php?r=measurement/delete&id=<?= $model->getId() ?>">Löschen</a>
        <a class="btn btn-default" href="index.php?r=home/index">Zurück</a>
    </p>

    <table class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>Time</th>
            <td><?= $model->getTime() ?></td>
        </tr>
        <tr>
            <th>Temperature</th>
            <td><?= $model->getTemperature() ?></td>
        </tr>
        <tr>
            <th>Rain</th>
            <td><?= $model->getRain() ?></td>
        </tr>
        <tr>
            <th>Station</th>
            <td><?= $model->getStation() -> getName() ?></td>
        </tr>
        </tbody>
    </table>
</div> <!-- /container -->