
<div class="container">
    <h2>Messdaten löschen</h2>
    <form class="form-horizontal" action="index.php?r=measurement/delete&id=<?= $model->getId() ?>" method="post">
        <input type="hidden" name="id" value="<?php echo $model->getId(); ?>"/>
        <p class="alert alert-error">Wollen Sie die Messdaten
            von <?= $model->getStation()->getName() ?> wirklich löschen?</p>
        <div class="form-actions">
            <button type="submit" class="btn btn-danger">Löschen</button>
            <a class="btn btn-default" href="index.php?r=home/index">Abbruch</a>
        </div>
    </form>

</div>
