<div class="container">
    <div class="row">
        <h2>Messdaten bearbeiten</h2>
    </div>
    <form class="form-horizontal" action="index.php?r=measurement/update&id=<?= $model->getId() ?>" method="post">
        <div class="row">
            <div class="col-md-5">
                <div class="form-group required <?= $model->hasError('time') ? 'has-error' : ''; ?>">
                    <label class="control-label">Time *</label>
                    <input type="datetime-local" class="form-control" name="time" maxlength="32"
                           value="<?= $model->getTime() ?>">

                    <?php if ($model->hasError('time')): ?>
                        <div class="help-block"><?= $model->getError('time') ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <div class="form-group required <?= $model->hasError('temperature') ? 'has-error' : ''; ?>">
                    <label class="control-label">Temperature *</label>
                    <input type="text" class="form-control" name="temperature" maxlength="128"
                           value="<?= $model->getTemperature() ?>">

                    <?php if ($model->hasError('temperature')): ?>
                        <div class="help-block"><?= $model->getError('temperature') ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required <?= $model->hasError('rain') ? 'has-error' : ''; ?>">
                    <label class="control-label">Rain *</label>
                    <input type="text" class="form-control" name="rain" maxlength="64"
                           value="<?= $model->getRain() ?>">

                    <?php if ($model->hasError('rain')): ?>
                        <div class="help-block"><?= $model->getError('rain') ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div>