$(document).ready(function ()
{
    loadAllMeasurement();
    chartFunc();

    $("#btnSearch").click(function () {
        loadFilteredMeasurement();
    });
});

/*
var xlabels = [];
var ytemos = [];
var ytemps2 = [];*/

function chartFunc(){
    //await getData(data);
    let date = [];
    let rain = [];
    let temp = [];
    var ctx = document.getElementById('chart').getContext('2d');



    $.get("api/measurement", function (data){

        $.each(data, function (index, measurement){

            date.push(measurement.time);
            rain.push(measurement.rain);
            temp.push(measurement.temperature);


        });
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: date,
                datasets: [{
                    label: 'Regenmenge in ml',
                    data: rain,
                    backgroundColor: ['rgba(3, 138, 255, 0.2)'],
                    borderColor: ['rgba(3, 138, 255, 1)'],
                    borderWidth: 1
                }, {
                    label: 'Temperatur in °C',
                    data: temp,
                    backgroundColor: ['rgba(250, 216, 89, 0.2)'],
                    borderColor: ['rgba(250, 216, 89, 1)'],
                    borderWidth: 1
                }]
            },

            // Configuration options go here
            options: {
                scales:{
                    yAxes:[{type: 'linear', position: 'left',
                        ticks:{
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

    });

    console.log(date)


}

function loadAllMeasurement() {
    $.get("api/measurement", function (data) {
        $("#measurements").html(parseMeasurementTable(data));
    });
}

function loadFilteredMeasurement() {

  var filter = $("#station_id").val();

    if (filter == "") {

       loadAllMeasurement();

    } else {
        $.get("api/station/"+ filter +"/measurement", function (data) {
            $("#measurements").html(parseMeasurementTable(data));

        });
}}

function parseMeasurementTable(data) {
    var tmp = "";
    $.each(data, function (index, measurement) {
        tmp += "<tr>";
        tmp += "<td>" + measurement.time + "</td>";
        tmp += "<td>" + measurement.temperature + "</td>";
        tmp += "<td>" + measurement.rain + "</td>";
        tmp += "<td>";
        tmp += '<a class="btn btn-info" href="index.php?r=measurement/view&id=' + measurement.id + '"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;'
        tmp += '<a class="btn btn-primary" href="index.php?r=measurement/update&id=' + measurement.id + '"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;'
        tmp += '<a class="btn btn-danger" href="index.php?r=measurement/delete&id=' + measurement.id + '"><span class="glyphicon glyphicon-remove"></span></a>';
        tmp += "</td>";
        tmp += "</tr>";
    });

    return tmp;
}

async function getData(data) {

    xlabels.length = 0;
    if (data !== undefined) {
        //console.log(data[1]);
        data.forEach(wert => {
            const date = wert['time'];
            //console.log(date);
            if (date !== undefined)
                xlabels.push(date);
            const temperature = wert['temperature'];
            if (temperature !== undefined)
                console.log(temperature);
            ytemps.push(temperature);
            const rain = wert['rain'];
            if (rain !== undefined)
                ytemps2.push(rain);
        })
    }
}